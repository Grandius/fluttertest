import 'package:flutter/material.dart';


class DegreesApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {


    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.brown,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Flutter layout demo test'),
        ),
        body: Column(
          children: [
            DegreesRow("Celsius"),
            DegreesRow("Fahrenheit")
          ],
        ),
      ),
    );
    //home: MyHomePage(title: 'Flutter Demo Home Page Test')
  }
}

class DegreesRow extends StatefulWidget {

  DegreesRow(this.name);

  final String name;

  @override
  _DegreesRow createState() => _DegreesRow(name);
}

// Define a corresponding State class.
// This class holds data related to the Form.

class _DegreesRow extends State<DegreesRow> {
  // Create a text controller. Later, use it to retrieve the
  // current value of the TextField.
  final myController = TextEditingController();
  final String name;

  _DegreesRow(this.name);

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    myController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
        padding: const EdgeInsets.all(32),
        child: Row(children: [
          Text(
            '$name',
            style: TextStyle(
              fontWeight: FontWeight.normal,
            ),
          ),
          Expanded(
              child:
              const TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Enter value',
                ),
              )
          )
        ]));

    return titleSection;
  }
}


